import { Component } from '@angular/core';
import { RecentTracks } from './nowListening';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  songTitle: string = '';
  artistName: string = '';
  recentTracks: RecentTracks = {};

  constructor(nowListeningService: NowListeningService) {
    nowListeningService.getNowListening().subscribe(
      (data: RecentTracks) => this.recentTracks = { ...data }
    );
  }

}

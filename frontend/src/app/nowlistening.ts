interface DateField {
  uts: string;
  #text: string;
}

interface Image {
  size: string;
  #text: string;
}

interface Artist {
  name: string;
  image: array<Image>;
  url: string;
}

interface Album {
  #text: string;
}

interface Track {
  mbid: string;
  loved: string;
  artist: Artist;
  image: array<Image>;
  date: DateField;
  url: string;
  name: string;
  album: Album;
}

interface TrackList {
  track: array<Track>;
}

interface RecentTracks{
  recenttracks: TrackList;
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable
class NowListeningService {
  constructor(private http: HttpClient) { }

  getNowListening() {
    return this.http.get('http://100.115.92.206:4000/now-listening');
  }
}


defmodule Nowlistening.Endpoint do
  use Plug.Router
  use Application
  import HTTPoison

  # Request pipeline
  plug(Plug.Logger)
  plug(:match)
  plug(Plug.Parsers, parsers: [:json], json_decoder: Poison)
  plug(:dispatch)

  # Route and view
  Plug.Router.get "/now-listening" do
    api_key = Application.get_env(:nowlistening, :api_key)
    url = "http://ws.audioscrobbler.com/2.0/"
    params = %{
      "method" => "user.getrecenttracks",
      "user" => "landreville",
      "limit" => 2,
      "extended" => 1,
      "api_key" => api_key,
      "format" => "json"
    }

    case HTTPoison.get(url, [], params: params) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        send_resp(conn, 200, body)
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect reason
        send_resp(conn, 500, "Server Error")
    end
  end

end

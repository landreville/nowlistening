defmodule Nowlistening.Application do
  use Application

  def start(_type, _args) do
    children = [
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: Nowlistening.Endpoint,
        options: [port: 4000]
      )
    ]

    opts = [strategy: :one_for_one, name: Nowlistening.Supervisor]
    Supervisor.start_link(children, opts)
  end
end

#!/bin/sh

set -e

mix deps.get

exec mix "$@"
